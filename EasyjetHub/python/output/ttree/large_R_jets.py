from EasyjetHub.output.ttree.branch_manager import BranchManager, SystOption
from EasyjetHub.output.ttree.truth_jets import get_TopHiggs_jet_truth_labels
from EasyjetHub.steering.sample_metadata import get_valid_ami_tag


def get_large_R_jet_branches(
    flags, tree_flags,
    input_container,
    output_prefix,
    lr_jet_type
):
    _syst_option = SystOption.ALL_SYST
    if flags.Analysis.disable_calib:
        _syst_option = SystOption.NONE

    large_R_jet_branches = BranchManager(
        input_container,
        output_prefix,
        systematics_option=_syst_option,
        systematics_suffix_separator=flags.Analysis.systematics_suffix_separator
    )

    if tree_flags.slim_variables_with_syst:
        large_R_jet_branches.syst_only_for = ["pt", "m"]

    if lr_jet_type == "Topo":
        large_R_jet_branches.required_flags.append(flags.Analysis.do_large_R_Topo_jets)
    if lr_jet_type == "UFO":
        large_R_jet_branches.required_flags.append(flags.Analysis.do_large_R_UFO_jets)

    large_R_jet_branches.add_four_mom_branches(do_mass=True)

    if flags.Analysis.do_overlap_removal:
        large_R_jet_branches.variables += ["passesOR_%SYS%"]

    if flags.Analysis.Large_R_jet.runMuonJetPtCorr:
        large_R_jet_branches.variables += ["uncorrPt", "n_muons"]

    if flags.Input.isMC and tree_flags.collection_options.large_R_jets.truth_labels:
        large_R_jet_branches.variables += [
            "GhostBHadronsFinalCount",
        ] + get_large_R_jet_truth_labels(flags)

        if lr_jet_type == "Topo":
            large_R_jet_branches.variables += [
                "R10TruthLabel_R21Consolidated",
            ]
        if lr_jet_type == "UFO":
            large_R_jet_branches.variables += [
                "GhostCHadronsFinalCount",
                "R10TruthLabel_R21Precision_2022v1",
                "R10TruthLabel_R22v1",
            ]

    if tree_flags.reco_outputs.VR_jets:
        large_R_jet_branches.variables += get_ghost_vr_branches(flags)

    if tree_flags.collection_options.large_R_jets.substructure_info:
        large_R_jet_branches.variables += get_substructure_branches(flags, lr_jet_type)

    if lr_jet_type == "UFO" and \
        flags.Analysis.Large_R_jet.wtag_type and \
            flags.Analysis.Large_R_jet.wtag_wp:
        large_R_jet_branches.variables += get_wtag_branches(flags)

    split_tags = flags.Input.AMITag.split("_")
    is_valid_ptag = get_valid_ami_tag(split_tags, "p", "p5834")
    if lr_jet_type == "UFO" and is_valid_ptag and \
       tree_flags.collection_options.large_R_jets.btag_details:
        large_R_jet_branches.variables += get_large_R_gn2_branches()

    return large_R_jet_branches.get_output_list()


def get_ghost_vr_branches(flags):
    vr_vars = [
        "goodVRTrackJets",
        "minRelativeDeltaRToVRJet",
        "leadingVRTrackJetsPt",
        "leadingVRTrackJetsEta",
        "leadingVRTrackJetsPhi",
        "leadingVRTrackJetsM",
        "leadingVRTrackJetsDeltaR12",
        "leadingVRTrackJetsDeltaR13",
        "leadingVRTrackJetsDeltaR32",
    ] + [
        f"leadingVRTrackJetsBtag_{wp}"
        for wp in flags.Analysis.Large_R_jet.vr_btag_wps
    ]
    if flags.Input.isMC:
        vr_vars += [
            "VRTrackJetsTruthLabel"
            # previously leadingVRTrackJets_HadronConeExclTruthLabelID
        ]
    return vr_vars


def get_substructure_branches(flags, lr_jet_type):
    substructure_vars = [
        "Tau1_wta",
        "Tau2_wta",
        "Tau3_wta",
        "ECF1",
        "ECF2",
        "ECF3",
        "Split12",
        "Split23",
    ]
    # Trimmed jet vars
    if lr_jet_type == "Topo":
        substructure_vars += [
            "NTrimSubjets",
            "TrackSumPt",
        ]
    if flags.Input.isPHYSLITE:
        substructure_vars += [
            "D2",
        ]
    return substructure_vars


def get_large_R_gn2_branches():
    gn2_branches = [
        "GN2Xv01_phbb",
        "GN2Xv01_phcc",
        "GN2Xv01_ptop",
        "GN2Xv01_pqcd",
    ]
    return gn2_branches


def get_large_R_jet_truth_labels(flags):
    parent_bosons = ["Higgs", "Scalar", "Top"]

    truth_labels = []
    if not flags.Input.isPHYSLITE:
        truth_labels += [
            f"parent{p}NMatchedChildren" for p in parent_bosons
        ]

    truth_labels += get_TopHiggs_jet_truth_labels(flags)

    return truth_labels


def get_wtag_branches(flags):
    wtag_type = flags.Analysis.Large_R_jet.wtag_type
    wtag_wp = flags.Analysis.Large_R_jet.wtag_wp

    if wtag_type == "DNN":
        wtag_branches = [
            f"{wtag_type}TaggerTopQuarkContained{wtag_wp}_Tagged",
            f"{wtag_type}TaggerTopQuarkContained{wtag_wp}_Score",
        ]

    else:
        wtag_branches = [
            f"{wtag_type}{wtag_wp}Tagger_Tagged",
            f"{wtag_type}{wtag_wp}Tagger_Score",
        ]

    return wtag_branches
